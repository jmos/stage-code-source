#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

void printfcomma (int n);

int main()
{
    FILE *file, *file_result;
    char c;
    int nb;
    int th0=0, th1=0, th2=0, th3=0, m=0;

    file = fopen("data", "r");

    if(file == NULL) {
        printf("ERROR FILE: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    file_result = fopen("results", "a"); //a = append data at the end of the file

    if(file_result == NULL) {
        printf("ERROR FILE: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    printf("File opened\n");
    printf("Starting analysis......\n");

    while(1){
        if(fscanf(file,"%c",&c) == EOF)
            break;

        if(isdigit(c)) {
            nb = c - '0';

            switch(nb) {
                case 0: th0++;
                break;

                case 1: th1++;
                break;

                case 2: th2++;
                break;

                case 3: th3++;
                break;

                default:
                break;
            }
        } else {
		if(c == 'M')
			m++;
	}
    }

    printf("Analysis over\n\n");
    printf("Thread 0....."); printfcomma(th0); printf(" repetitions\n");
    printf("Thread 1....."); printfcomma(th1); printf(" repetitions\n");
    printf("Thread 2....."); printfcomma(th2); printf(" repetitions\n");
    printf("Thread 3....."); printfcomma(th3); printf(" repetitions\n");
    printf("Main........."); printfcomma(m);   printf(" repetitions\n");
    printf("---------------------------\n");
    printf("Total........"); printfcomma(th0+th1+th2+th3+m); printf("\n");

	fprintf(file_result, "\n\n*************RESULTS**************\n");
    fprintf(file_result, "      Thread 0... %d\n", th0);
    fprintf(file_result, "      Thread 1... %d\n", th1);
    fprintf(file_result, "      Thread 2... %d\n", th2);
    fprintf(file_result, "      Thread 3... %d\n", th3);
	fprintf(file_result, "      Main....... %d\n", m);
    fprintf(file_result, "    ---------------------------\n");
    fprintf(file_result, "      Total...... %d\n", th0+th1+th2+th3+m);

    fclose(file);
    fclose(file_result);


    system("./save_results.sh");
    printf("Results file saved in /results_saved/preempt_kernel\n");

    return EXIT_SUCCESS;
}

void printfcomma (int n) {
    if (n < 0) {
        printf ("-");
        printfcomma (-n);
        return;
    }
    if (n < 1000) {
        printf ("%d", n);
        return;
    }
    printfcomma (n/1000);
    printf (",%03d", n%1000);
}
