#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include <time.h>

#define THREADS_NB      4
#define SIMULATION_TIME 10    //in seconds

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
FILE *file, *file_results;
int timer_terminate = 0;

void *thread_fct(void *arg)
{
    int a = (int) arg;
    struct sched_param param_thread;
    int policy;

    pthread_mutex_lock(&mutex);
    //SET PARAMS
    param_thread.__sched_priority = sched_get_priority_max(SCHED_FIFO) - 1;
    pthread_setschedparam(pthread_self(), SCHED_FIFO, &param_thread);

    //SET TO 0 (just to make sure the getschedparam() returns the actual number
    param_thread.__sched_priority = 0;

    //VERIFY PARAMS
    pthread_getschedparam(pthread_self(), &policy, &param_thread);

    printf("thread ID: %d ; arg: %d\n", (int)pthread_self(), a);
    printf("Scheduler (obtain w/ PID): %d\n", sched_getscheduler(getpid()));
    printf("Policy (obtain w/ thread): %d\n", policy);
    printf("Priority: %d\n", param_thread.__sched_priority);
    printf("------------------------------\n");

    fprintf(file_results, "thread ID: %d ; arg: %d\n", (int)pthread_self(), a);
    fprintf(file_results, "Scheduler (obtain w/PID): %d\n", sched_getscheduler(getpid()));
    fprintf(file_results, "Policy (obtain w/ thread): %d\n", policy);
    fprintf(file_results, "Priority: %d\n", param_thread.__sched_priority);
    fprintf(file_results, "------------------------------\n");
    pthread_mutex_unlock(&mutex);

    while(!timer_terminate)
        fprintf(file, "%d", a);

    pthread_exit(NULL);
}

int main()
{
    int i, arg;
    pthread_t pthread[THREADS_NB];
    struct sched_param param;

    time_t timer_start, timer_end;
    double elapsed;

    printf("Opening files....\n");
    file = fopen("data", "w");
    file_results = fopen("results", "w");
    if(file == NULL || file_results == NULL) {
        printf("FILE ERROR: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    if(pthread_mutex_init(&mutex, NULL) != 0) {
        printf("Error mutex: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

	param.sched_priority = 85;

	if(sched_setscheduler(getpid(), SCHED_FIFO, &param) != 0) {
		printf("Error setting scheduler: %s\n", strerror(errno));
		return -1;
	}

    sched_getparam(getpid(), &param);

    printf("FIFO: %d. OTHER: %d\n", SCHED_FIFO, SCHED_OTHER);
    printf("Scheduler in main: %d\n", sched_getscheduler(getpid()));
    printf("Priority in main: %d\n", param.__sched_priority);
    printf("------------------------------\n");

    fprintf(file_results, "FIFO: %d. OTHER: %d\n", SCHED_FIFO, SCHED_OTHER);
    fprintf(file_results, "Scheduler in main: %d\n", sched_getscheduler(getpid()));
    fprintf(file_results, "Priority in main: %d\n", param.__sched_priority);
    fprintf(file_results, "Simulation time: %d [s]\n", SIMULATION_TIME);
    fprintf(file_results, "------------------------------\n");


    for(i=0; i<THREADS_NB; i++) {
        arg = i;

        if (pthread_create(&pthread[i], NULL, thread_fct, (void*)arg) != 0) {
            printf("THREAD CREATING ERROR: %s\n", strerror(errno));
            return EXIT_FAILURE;
        }
    }

    timer_start = time(NULL);

    while (!timer_terminate) {
        timer_end = time(NULL);
        elapsed = difftime(timer_end, timer_start);
        if (elapsed >= SIMULATION_TIME)
            timer_terminate = 1;

        fprintf(file, "M");
    }

    for(i=0; i<THREADS_NB; i++) {
        pthread_join(pthread[i], NULL);
    }

    pthread_mutex_destroy(&mutex);

    printf("Closing files...\n");
    fclose(file);
    fclose(file_results);

    printf("End of main\n");

    return EXIT_SUCCESS;
}
