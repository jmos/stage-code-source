#include "leds.h"

int led_export(int led_number)
{
    FILE *f = NULL;
    char path[100];

    f = fopen("/sys/class/gpio/export", "w");
    if(f == NULL) {
        printf("FILE EXPORT: %s\n", strerror(errno));
        return -1;
    }

    fprintf(f, "%d", led_number);
    fclose(f);

    snprintf(path, sizeof(path), "/sys/class/gpio/gpio%d/direction", led_number);
    f = fopen(path, "w");
    if(f == NULL) {
        printf("FILE DIRECTION %d: %s\n", led_number, strerror(errno));
        printf("    path was: %s\n", path);
        return -1;
    }
    fprintf(f, "out");

    fclose(f);

    return 0;
}

int led_unexport(int led_number)
{
    FILE *f = NULL;

    f = fopen("/sys/class/gpio/unexport", "w");
    if(f == NULL) {
        printf("FILE UNEXPORT: %s\n", strerror(errno));
        return -1;
    }

    fprintf(f, "%d", led_number);
    fclose(f);

    return 0;
}

int led_set_value(s_led *led)
{
    FILE *f;
    char buf[100];

    snprintf(buf, sizeof(buf), "/sys/class/gpio/gpio%d/value", led->number);
    f = fopen(buf, "r+");
    if(f == NULL) {
        printf("FILE VALUE GPIO%d: %s\n", led->number, strerror(errno));
        return -1;
    }

    fprintf(f, "%d", led->state);
    fclose(f);

    return 0;
}

