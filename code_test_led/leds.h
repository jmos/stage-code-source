#ifndef LEDS_H_INCLUDED
#define LEDS_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "struct.h"

int led_export(int led_number);

int led_unexport(int led_number);

int led_set_value(s_led *led);


#endif // LEDS_H_INCLUDED

