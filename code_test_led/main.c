#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include "leds.h"
#include "struct.h"

/*
    /!\ add -lpthread to the build/linker options
    /!\ launch with sudo to allow changing priorities
*/

#define THREADS_NB          4
int SIMULATION_TIME =       4;

int timer_over=0;
s_data_threads threads[THREADS_NB];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int led_on=1;


void *thread_fct(void *thread_arg)
{
    int itself, ret_value, policy;
    struct sched_param param_thread;

    for(itself=0; itself<THREADS_NB; itself++)
    {
        if(threads[itself].thread_id == pthread_self())
        {
            break;
        }
    }

    param_thread.__sched_priority = sched_get_priority_max(SCHED_FIFO) - itself - 1;
    ret_value = pthread_setschedparam(pthread_self(), SCHED_FIFO, &param_thread);

    if(ret_value != 0)
        printf("Thread %d, ret value = %d, strerror : %s\n", itself, ret_value, strerror(ret_value));


    param_thread.__sched_priority = 0;
    pthread_getschedparam(pthread_self(), &policy, &param_thread);
    printf("THREAD: %d    GPIO: %d    PRIORITY: %d  ID: %d\n", itself,
                                                                threads[itself].led.number,
                                                                param_thread.__sched_priority,
                                                                (int)threads[itself].thread_id);

    while(!timer_over)
    {

        pthread_mutex_lock(&mutex);

        if(led_on != itself)
        {
            threads[led_on].led.state = 0;
            led_set_value(&threads[led_on].led);
            threads[itself].led.state = 1;
            led_set_value(&threads[itself].led);
            led_on = itself;
        }
        pthread_mutex_unlock(&mutex);

/*
      //4 LEDS BLINKING

        threads[itself].led.state = ! threads[itself].led.state;

        led_set_value(&threads[itself].led);

        usleep(500); //in [us]
*/
    }

    pthread_exit(NULL);
}

int main(int argc, char **argv)
{
    int i;
    struct sched_param param;
    time_t timer_start, timer_now;
    double elapsed;
    pthread_mutexattr_t mutex_attr;

    int led_number[] = {22, 26, 20, 21};

    for(i=0; i<4; i++)
    {
        threads[i].led.state = 0;
        threads[i].led.number = led_number[i];
    }

    if (argc == 2)
    {
        SIMULATION_TIME = strtol(argv[1], NULL, 10);
    }

    /*
        LED EXPORT
    */
    for(i=0; i<4; i++)
    {
        if(led_export(threads[i].led.number) != 0)
        {
            printf("Aborted program because of led %d, gpio%d\n", i, threads[i].led.number);
            return -1;
        }
        printf("GPIO%d set up\n", threads[i].led.number);
    }
    printf("------------------------------\n");

    /*
        MUTEX
    */
    if(pthread_mutexattr_init(&mutex_attr) != 0)
    {
        printf("Error mutex attr init: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    if(pthread_mutexattr_setprioceiling(&mutex_attr, sched_get_priority_max(SCHED_FIFO)) != 0)
    {
        printf("Error mutex attr setprioceiling: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    if(pthread_mutex_init(&mutex, &mutex_attr) != 0)
    {
        printf("Error mutex: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    /*
        SCHED
    */
    param.__sched_priority = 80;

    if(sched_setscheduler(getpid(), SCHED_FIFO, &param) != 0)
    {
        printf("Error setting scheduler: %s\n", strerror(errno));
        return -1;
    }

    sched_getparam(getpid(), &param);

    printf("Scheduler in main: %s\n", (sched_getscheduler(getpid()) == SCHED_FIFO) ? "FIFO" : "OTHER");
    printf("Priority in main: %d\n", param.__sched_priority);
    printf("------------------------------\n");

    /*
        THREADS
    */
    printf("Launching the threads...\n\n\n");
    for(i=0; i<4; i++)
    {
        if (pthread_create(&threads[i].thread_id, NULL, thread_fct, NULL) != 0)
        {
            printf("Thread %d, creating error: %s\n", i, strerror(errno));
            return EXIT_FAILURE;
        }
    }

    /*
        WORKING
    */
    timer_start = time(NULL);
    while (!timer_over)
    {
        timer_now = time(NULL);
        elapsed = difftime(timer_now, timer_start);
        if (elapsed >= SIMULATION_TIME)
            timer_over = 1;
    }

    /*
        END OF PROGRAM
    */
    for(i=0; i<4; i++)
    {
        pthread_join(threads[i].thread_id, NULL);
    }

    for(i=0; i<4; i++)
    {
        threads[i].led.state = 0;
        led_set_value(&threads[i].led);

        //ERROR FOR NEXT TIME IF UNEXPORTED
        //led_unexport(leds[i]);
    }

    pthread_mutex_destroy(&mutex);
    pthread_mutexattr_destroy(&mutex_attr);


    return 1;
}
