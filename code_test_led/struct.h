#ifndef STRUCT_H_INCLUDED
#define STRUCT_H_INCLUDED

typedef struct s_led
{
    int number;
    int state;
} s_led;

typedef struct s_data_threads
{
    pthread_t thread_id;
    s_led led;
} s_data_threads;

#endif // STRUCT_H_INCLUDED
