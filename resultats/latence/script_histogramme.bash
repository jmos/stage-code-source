#!/bin/bash

# 1. Run cyclictest
#cyclictest -l100000000 -m -Sp90 -i200 -h400 -q >output 

# 2. Get maximum latency
max=`grep "Max Latencies" output | tr " " "\n" | sort -n | tail -1 | sed s/^0*//`

# 2bis. latence moyenne
avg=`grep "Avg Latencies" output | tr " " "\n" | sort -n | tail -1 | sed s/^0*//`

# 3. Grep data lines, remove empty lines and create a common field separator
grep -v -e "^#" -e "^$" output | tr " " "\t" >histogram 

# 4. Set the number of cores, for example
cores=4

# 5. Create two-column data sets with latency classes and frequency values for each core, for example
for i in `seq 1 $cores`
do
  column=`expr $i + 1`
  cut -f1,$column histogram >histogram$i
done

# 5bis. Mise en forme du fichier contenant la temperature
sed "s/'//g" temp > temp_without_quote &&
sed "s/C//g" temp_without_quote > temp &&
rm temp_without_quote

# 6. Create plot command header
echo -n -e "set title \"Latency plot\"\n\
set terminal png\n\
set xlabel \"Latency (us), max $max us, avg $avg us\"\n\
set logscale y\n\
set xrange [0:400]\n\
set yrange [0.8:*]\n\
set ylabel \"Number of latency samples\"\n\
set y2label \"Celsius\"\n\
set y2tics\n\
set tics out\n\
set y2range [45:75]\n\
set output \"plot.png\"\n\
plot " >plotcmd

# 7. Append plot command data references
for i in `seq 1 $cores`
do
  if test $i != 1
  then
    echo -n ", " >>plotcmd
  fi
  cpuno=`expr $i - 1`
  if test $cpuno -lt 10
  then
    title=" CPU$cpuno"
   else
    title="CPU$cpuno"
  fi
  echo -n "\"histogram$i\" using 1:2 title \"$title\" with histeps" >>plotcmd
done

# 7bis. Ajout de la temperature
echo -n ", \"temp\" title \"temperature\" axis x2y2 with histeps" >>plotcmd

# 8. Execute plot command
gnuplot -persist <plotcmd && rm histogram*
